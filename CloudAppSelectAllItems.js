/*
 * Checks all list items on http://getcloudapp.com upload list,
 *  which is useful for multiple item deletion (AKA nuke the crap of all the uploads)
 * 
 * License:
 * José Moreira, in 2013 and beyond, doesn't care what you do with this piece of code.
 * 
 */

(function(){
    var items = $$('ol#listing input[type="checkbox"]');
    if (items.length && items.length >= 0) {
        //  check all items
        var i;
        for (i=0; i < items.length; i++) {
            items[i].click();
        }
    }
})();

// #1 click "Delete Selected"
// #2 ...
// #3 Profit !
// #4 Refresh page or whatever
// #5 GOTO 1 
